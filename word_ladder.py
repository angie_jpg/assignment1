import re
import os

def same(item, target): #returns two strings and generates a list of all individual characters in same position, returns length
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list): #returns list of words with one letter difference
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

def find(word, words, seen, target, path, prevMatch): #function for getting the desired path (shortest path)
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list) #builds list of words within pattern and ignores repetition of words within list
  if len(list) == 0: #ends function if there are no words left to build with
    return False
  list = sorted([(same(w, target), w) for w in list])[::-1] #Generates a list of tuples containing words in dictionary and target word
  (match, item) = list[0]
  for (match, item) in list: #ends function if there is a final word in list before target word
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item) #appends word before target word
      return True
    seen[item] = True #adds words from dictionary as seen
  for (match, item) in list: #iterates through list, adds next item
    path.append(item)
    if match >= prevMatch and find(item, words, seen, target, path, match): #HERE
      return True
    path.pop()

def fileInput(fname):
  ext = os.path.splitext(fname)[-1].lower() #parameters for file extension
  if len(fname) == 0: #checks if filename has no strings
    print("Error: Filename not given")
    return False
  elif len(fname) > 64: #checks if filename is over 64 characters long
    print("Error: Filename is too long. Must be 64 characters long or less")
    return False
  elif len(ext) < 4 or len(ext) > 5: #checks if file extension is 3-4 characters long (. included)
    print("Error: File extension should be 3-4 characters long")
    return False
  elif not os.path.exists(fname): #checks if file name is non-existent
    print("Error: File does not exist")
    return False
  else:
    file = open(fname)
    lines = file.readlines()
    if len(lines) == 0: #checks if there is a blank file
      print("Error: File is empty")
      file.close()
      return False
    return lines
    

def WordInput(start, target, dicts):
  words = []
  for line in dicts:
    word = line.rstrip()
    if len(word) == len(start): #checks if words are the same length as the start word
      words.append(word)
  if not start.isalpha(): #checks if there are any numbers within the start input or if it is blank
    print("Error: Input cannot be blank or include numbers")
    return False
  if start not in words: #finds whether or not the start word is in the file
    print("Error: Word not in dictionary. Please try again")
    return False
  if not target.isalpha(): #checks if there are any numbers within the start input or if it is blank
    print("Error: Input cannot be blank or include numbers")
    return False
  if len(target) != len(start): #checks whether or not the start word is the same length as the target word
    print("Error: Target word must be the same length of start word")
    return False
  if target not in words: #finds whether or not the target word is in the file
    print("Error: Word not in dictionary. Please try again")
    return False
  if target == start:  # checks to see if start and target word are the same
    print("Error: start word cannot be the target word")
    return False
  return words

fname = input("Enter dictionary name: ") #gets file input from user
dicts = fileInput(fname) #calls the function
if not dicts: #if any error happens with file name inputs
  exit(0)
start = input("Enter start word:") #user inputs target word
target = input("Enter target word:") #user inputs target word
words = wordInput(start, target, dicts) #calls function
if not words: #if any error happens with word inputs
  exit(0)
middle = input("Enter a word that must exist (optional):") #gets input for mandatory word
banned = input("Enter banned word (optional):") #gets input for blacklisted words



count = 0
path = [start]
seen = {start: True}
for word in banned.split(","):                              #       split converts banned into a list and adds a "," to seperate the words
  seen[word] = True

if middle:                                                  #       If middle exists, run the code to find start > middle > target.
    if find(start, words, seen, middle, path, 0):           #       this highlights the path to the middle word
      path.append(middle)
    else:
      print("No path found")                                #       Prints no path found if it can't be located
    if find(middle, words, seen, target, path, 0):          #       This finds the path from middle to target and prints
      path.append(target)
      print(len(path) - 1, path)
    else:
      print("No path found")                                #       Prints no path found if it can't be located
else:                                                       #       Else run start > target and print
    if find(start, words, seen, target, path, 0):
        path.append(target)
        print(len(path) - 1, path)
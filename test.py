import word_ladder
import unittest

dicts = ["hide", "seek", "site", "side", "go", "sits", "sies", "sees"]

class TestInputErrorHandling(unittest.TestCase):

    def test_GetFileInputInvalid(self): #tests invalid file
        self.assertFalse(word_ladder.fileInput("x.txt"))

    def test_GetFileInputBlank(self): #tests if there is no input
        self.assertFalse(word_ladder.fileInput(""))

    def test_GetFileInputEmpty(self): #tests empty file
        self.assertFalse(word_ladder.fileInput("empty.txt"))

    def test_GetFileExtensionInvalid(self): #tests invalid file extension
        self.assertFalse(word_ladder.fileInput("dictionary.toast"))

    def test_GetWordsValid(self): #correct inputs for start and target word
        self.assertTrue(word_ladder.wordInput("hide", "seek", dicts))

    def test_GetWordsEmptyStart(self): #checks if there is no input for start word
        self.assertTrue(word_ladder.wordInput("", "seek", dicts))

    def test_GetWordsEmptyTarget(self): #checks if there is no input for start word
        self.assertTrue(word_ladder.wordInput("hide", "", dicts))

    def test_GetWordsInvalidLength(self): #tests length of start and target words
        self.assertFalse(word_ladder.wordInput("hide", "go", dicts))

    def test_GetWordsInvalidStart(self): #tests if start word is not in dictionary
        self.assertFalse(word_ladder.wordInput("hyde", "seek", dicts))

    def test_GetWordsInvalidTarget(self): #tests if target word is not in dictionary
        self.assertFalse(word_ladder.wordInput("hide", "seak", dicts))

    def test_GetWordsIntStart(self): #checks any numbers are input within the start
        self.assertFalse(word_ladder.wordInput("123", "seek", dicts))

    def test_GetWordsIntTarget(self): #checks any numbers are input within the target
        self.assertFalse(word_ladder.wordInput("hide", "987", dicts))

    def test_GetWordsEmptyInput(self): #checks if start input is the same as the target input
        self.assertFalse(word_ladder.wordInput("seek", "seek", dicts))

class TestLadder(unittest.TestCase):
    def testSame(self): #tests same function
        item = "mead"
        target = "good"
        self.assertEqual(word_ladder.same(item, target), 1)

    def testBuild(self): #tests build function
        pattern = "le.d"
        words = ['kiss', 'just', 'mood', 'lead', 'lend', 'weed']
        seen = {"blob": True, "slob": True, "lead": True}
        list = []
        expected = ['lend']
        self.assertEqual(expected, word_ladder.build(pattern, words, seen, list))

    def testFind(self): #tests find function
            start = "lead"
            target = "gold"
            path = [start]
            seen = {start: True}
            fname = "dictionary.txt"
            file = open(fname)
            lines = file.readlines()
            words = []
            for line in lines:
                word = line.rstrip()
                if len(word) == len(start):
                    words.append(word)
            found = word_ladder.find("lead", words, seen, target, path, 0)
            path.append(target)
            self.assertTrue(found)
            self.assertEqual(path, ["lead", "load", "goad", "gold"])


if __name__ == "__main__":
    unittest.main()